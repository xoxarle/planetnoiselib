package planetnoiselib

import (
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestNewTextureBuilder(t *testing.T) {
	tb, err := NewTextureBuilder()
	expected := (err == nil)
	if !expected {
		t.Errorf("Should not return error")
	}
	expected = (tb != nil)
	if !expected {
		t.Errorf("Should return an object")
	}
	tb.Free()

}

func TestCreateParamTexture(t *testing.T) {
	err := CreateParamTexture(true, "Perlin", "Billow", 0.2, 0.8, 640, "./_test_out", "TestCreateParamTexture", "")
	expected := (err == nil)
	if !expected {
		t.Errorf("Should not return error")
	}

}

func TestCreateTexture(t *testing.T) {
	res, err := CreateTexture("./_test_out/TestCreateParamTexture.texjson", "./_test_out", "TestCreateTexture")
	expected := (err == nil)
	if !expected {
		t.Errorf("Should not return error")
	}
	if res == "" {
		t.Errorf("Should not return empty string")
	}

}

func TestJsonPreset(t *testing.T) {
	res, err := GetJsonPreset("Pippo")
	expected := (err != nil)
	if !expected {
		t.Errorf("Should return error when getting json preset \"pippo\" ")
	}
	res = PlanetTemplates[JsonPresets[0]]
	if res == "" {
		t.Errorf(fmt.Sprintf("GetJsonPreset(\"%s\") should not return empty string\n", JsonPresets[0]))
	}
}

func TestAllPresets(t *testing.T) {
	l := len(JsonPresets)
	if l <= 0 {
		t.Errorf(fmt.Sprintf("Should exist more than one preset, instead got %d", l))
	}
	for ix, preset := range JsonPresets {
		sName := fmt.Sprintf("Preset_%d", ix)
		fmt.Println("Testing preset...", preset, " output on name: ", sName)
		res := PlanetTemplates[preset]
		if res == "" {
			t.Errorf(fmt.Sprintf("GetJsonPreset(\"%s\") should not return empty string\n", preset))
		}
		tb, err := NewTextureBuilder()
		defer tb.Free()
		expected := (err == nil)
		if !expected {
			t.Errorf("Should not return error")
		}
		expected = (tb != nil)
		if !expected {
			t.Errorf("Should return an object")
		}
		err = tb.SetOutputName(sName)
		if err != nil {
			t.Errorf("Setting an output name should not fail")
		}
		err = tb.SetSize(1280, 640)
		if err != nil {
			t.Errorf("Setting a correct size should fail")
		}
		err = tb.LoadTextureFromJsonString(res)
		err = tb.RenderTexture("./_test_out")
		if err != nil {
			t.Errorf(fmt.Sprintf("If you arrived here rendering should not fail %v", err))
		}
	}
}

func TestCreateComponent(t *testing.T) {
	tb, err := NewTextureBuilder()
	if err != nil {
		t.Errorf("NewTextureBuilder should not fail")
	}

	err = tb.SetOutputName("")
	if err == nil {
		t.Errorf(fmt.Sprintf("Setting an empty output name should fail - %v", err))
	}

	err = tb.SetOutputName("TestCreateComponent")
	if err != nil {
		t.Errorf("Setting an output name should not fail")
	}

	err = tb.SetSize(-1, -1)
	if err == nil {
		t.Errorf(fmt.Sprintf("Setting a negative size should fail - %v", err))
	}

	err = tb.SetSize(1280, 640)
	if err != nil {
		t.Errorf("Setting a correct size should fail")
	}

	err = tb.Billow("billow", -1, 1.5, 0.5, 0.5, 8, true)
	if err != nil {
		t.Errorf(fmt.Sprintf("Setting an output generator name should not fail %v", err))
	}

	err = tb.Perlin("billow", -1, 0.5, 0.5, 0.5, 8, true)
	if err == nil {
		t.Errorf("Setting an output generator name already used should fail")
	}

	err = tb.Perlin("billow2", 1, 3.5, 0.5, 0.5, 8, true)
	if err != nil {
		t.Errorf("Setting an output generator with a new name should not fail")
	}

	err = tb.Avg("avg", "billow", "billow2", false)
	if err != nil {
		t.Errorf("Setting existent source generators should not fail")
	}

	err = tb.CreateImageDescriptor("")
	if err == nil {
		t.Errorf("Setting an empty image name should fail")
	}

	err = tb.CreateImageDescriptor("Image")
	if err != nil {
		t.Errorf("Creating a non-empty  image name should not fail")
	}

	err = tb.CreateHeightmapDescriptor("Heightmap")
	if err != nil {
		t.Errorf("Creating a non-empty  height map should not fail")
	}

	err = tb.CreateHeightmapDescriptor("Heightmap")
	if err == nil {
		t.Errorf("Recreating the same heightmap should fail")
	}

	err = tb.CreateNoiseMapBuilderDescriptor("", "", "")
	if err == nil {
		t.Errorf("Creating a NoiseMapBuilder with empty params should fail")
	}

	err = tb.CreateNoiseMapBuilderDescriptor("HMB", "", "")
	if err == nil {
		t.Errorf("Creating a NoiseMapBuilder with name but empty params should fail")
	}

	err = tb.CreateNoiseMapBuilderDescriptor("HMB", "avg", "gaa")
	if err == nil {
		t.Errorf("Creating a NoiseMapBuilder with name but non-exising module and heightmap")
	}

	err = tb.CreateNoiseMapBuilderDescriptor("HMB", "goo", "Heightmap")
	if err == nil {
		t.Errorf("Creating a NoiseMapBuilder with name but non-exising module and heightmap / 2")
	}

	err = tb.CreateNoiseMapBuilderDescriptor("HMB", "avg", "Heightmap")
	if err != nil {
		t.Errorf("Creating a NoiseMapBuilder with name and existing module and heightmap should not fail")
	}

	gi := []GradientInfo{
		{Position: -1.0, Red: 0, Green: 0, Blue: 0, Alpha: 255},
		{Position: 1.0, Red: 255, Green: 255, Blue: 255, Alpha: 255},
	}

	err = tb.CreateRendererDescriptor(
		"",
		"",
		"",
		"",
		"",
		"",
		2.0,
		2.0,
		true,
		true,
		gi,
		&ColorInfo{Red: 255, Blue: 255, Green: 255, Alpha: 255},
	)
	if err == nil {
		t.Errorf("You should specify the Renderer's name, the Heightmap's name and at least the background image name")
	}

	err = tb.CreateRendererDescriptor(
		"Renderer",
		"Heightmapz",
		"Imagez",
		"",
		"",
		"",
		2.0,
		2.0,
		true,
		true,
		gi,
		&ColorInfo{Red: 255, Blue: 255, Green: 255, Alpha: 255},
	)
	if err == nil {
		t.Errorf("All heightmaps and images must exist")
	}

	err = tb.CreateRendererDescriptor(
		"Renderer",
		"Heightmap",
		"Image",
		"",
		"",
		"",
		2.0,
		2.0,
		true,
		true,
		gi,
		&ColorInfo{Red: 255, Blue: 255, Green: 255, Alpha: 255},
	)
	if err != nil {
		t.Errorf(fmt.Sprintf("All heightmaps and images must exist %v", err))
	}

	err = tb.RenderTexture("./_test_out")
	if err != nil {
		t.Errorf(fmt.Sprintf("If you arrived here rendering should not fail %v", err))
	}

	err = tb.Free()
	if err != nil {
		t.Errorf("Freeing an allocated TextureBuilder object should not fail")
	}

}

func TestCreateWord(t *testing.T) {
	res := CreateWord()
	expected := (res != "")
	if !expected {
		t.Errorf("Should return a non-zero length word instead of empty string")
	}
}

func TestCreateComplexTexture(t *testing.T) {

	rand.Seed(time.Now().UnixNano())

	fmt.Println("TEXTURONA!")

	tb, err := NewTextureBuilder()
	err = tb.Cylinders("mod1", 1.3, false)
	err = tb.Spheres("mod2", 1.3, false)
	err = tb.Perlin("mod3", -1, 1.5, 2.5, 0.5, 8, false)
	err = tb.Billow("mod4", -1, 1.5, 2.5, 0.5, 8, false)
	err = tb.RidgedMulti("mod5", -1, 1.1, 3.1, 8, false)
	err = tb.RidgedMulti2("mod6", -1, 1.1, 2.1, 0.8, 0.9, 0.1, 8, false)
	err = tb.Voronoi("mod7", -1, 1.3, 0.01, true, false)
	err = tb.Checkerboard("mod8")

	err = tb.Curve("ed11", "mod3", []CPointStruct{
		{-1.0, 1.0},
		{-0.5, 0.3},
		{0.0, -1.0},
		{0.5, -0.3},
		{1.0, 1.0},
	}, false)
	err = tb.Terrace("ed12", "mod4", []CPointStruct{
		{-1.0, -1.0},
		{-0.5, 0.5},
		{0.0, 0.0},
		{0.5, 0.5},
		{1.0, 1.0},
	}, true, false)

	err = tb.Displace("ed20", "mod1", "mod2", "mod3", "mod4", "ed11", false)
	err = tb.Displace("ed21", "mod5", "mod6", "mod7", "mod4", "ed12", false)

	/*
		err = tb.Avg("ed30", "ed11", "ed20", false)
		err = tb.Abs("ed01", "mod3", false)
		err = tb.Add("ed02", "mod1", "mod2", false)
		err = tb.Multiply("ed03", "mod4", "mod5", false)
		err = tb.Min("ed04", "mod2", "mod7", false)
		err = tb.Avg("ed05", "mod5", "mod1", false)
		err = tb.Max("ed06", "mod6", "mod2", false)
		err = tb.Blend("ed07", "ed05", "mod8", "mod4", false)
		err = tb.Clamp("ed08", "mod4", 0.5, -0.5, false)
		err = tb.Cos("ed09", "ed08", 0.25, 1.2, 1.0, false)
		err = tb.Sin("ed10", "ed06", 0.25, 1.2, 1.0, false)
		err = tb.Displace("ed20", "ed01", "ed02", "ed03", "ed04", "ed05", false)
		err = tb.Displace("ed21", "ed06", "ed07", "ed08", "ed09", "ed10", false)
		err = tb.Avg("ed30", "ed11", "ed20", false)
		err = tb.Avg("ed31", "ed12", "ed21", false)
	*/

	err = tb.CreateImageDescriptor("Color")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}
	err = tb.CreateImageDescriptor("Bump")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}
	err = tb.CreateImageDescriptor("Spec")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}

	err = tb.CreateHeightmapDescriptor("HColor")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}
	err = tb.CreateHeightmapDescriptor("HBump")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}
	err = tb.CreateHeightmapDescriptor("HSpec")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}

	err = tb.CreateNoiseMapBuilderDescriptor("BColor", "ed20", "HColor")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}
	err = tb.CreateNoiseMapBuilderDescriptor("BBump", "ed21", "HBump")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}
	err = tb.CreateNoiseMapBuilderDescriptor("BSpec", "ed12", "HSpec")
	if err != nil {
		t.Errorf("Error %v\n", err)
	}

	giBump := []GradientInfo{
		{Position: -1.0, Red: 0, Green: 0, Blue: 0, Alpha: 255},
		{Position: 1.0, Red: 255, Green: 255, Blue: 255, Alpha: 255},
	}

	giSpec := []GradientInfo{
		{Position: -1.0, Red: 224, Green: 224, Blue: 224, Alpha: 255},
		{Position: 1.0, Red: 64, Green: 64, Blue: 64, Alpha: 255},
	}

	giColor := []GradientInfo{
		{Position: -1.0, Red: 154, Green: 102, Blue: 66, Alpha: 255},
		{Position: 0.0, Red: 202, Green: 164, Blue: 114, Alpha: 255},
		{Position: 0.5, Red: 105, Green: 52, Blue: 42, Alpha: 255},
		{Position: 1.0, Red: 64, Green: 79, Blue: 36, Alpha: 255},
	}

	err = tb.CreateRendererDescriptor(
		"RColor",
		"HColor",
		"Color",
		"",
		"",
		"",
		2.0,
		2.0,
		false,
		true,
		giColor,
		&ColorInfo{Red: 255, Blue: 255, Green: 255, Alpha: 255},
	)
	if err != nil {
		t.Errorf("All heightmaps and images must exist")
	}

	err = tb.CreateRendererDescriptor(
		"RBump",
		"HBump",
		"Bump",
		"",
		"",
		"",
		2.0,
		2.0,
		false,
		false,
		giBump,
		&ColorInfo{Red: 255, Blue: 255, Green: 255, Alpha: 255},
	)
	if err != nil {
		t.Errorf(fmt.Sprintf("All heightmaps and images must exist %v", err))
	}

	err = tb.CreateRendererDescriptor(
		"RSpec",
		"HSpec",
		"Spec",
		"",
		"",
		"",
		2.0,
		2.0,
		false,
		false,
		giSpec,
		&ColorInfo{Red: 255, Blue: 255, Green: 255, Alpha: 255},
	)
	if err != nil {
		t.Errorf(fmt.Sprintf("All heightmaps and images must exist %v", err))
	}

	err = tb.SetColorMap("Color")
	err = tb.SetBumpMap("Bump")
	err = tb.SetSpecMap("Spec")

	fmt.Println("Stop per renderizzare")

	tb.AddRandomFactors([]float64{0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8})
	tb.SetOutputName("COMPLEX")

	err = tb.RenderTexture("./_test_out")
	if err != nil {
		t.Errorf(fmt.Sprintf("If you arrived here rendering should not fail %v", err))
	}

	err = tb.Free()
	if err != nil {
		t.Errorf("Freeing an allocated TextureBuilder object should not fail")
	}

}
