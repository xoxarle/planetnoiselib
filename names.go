package planetnoiselib

import "math/rand"

var VOWELS = []string{"a", "e", "i", "o", "u", "y"}
var CONS = []string{"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "r", "s", "t", "v", "w", "x", "z"}

func v() string {
	n := rand.Intn(len(VOWELS))
	return VOWELS[n]
}

func c() string {
	n := rand.Intn(len(CONS))
	return CONS[n]
}

var dyp = []string{
	"ea", "ea", "ea", "ea",
	"ei", "ei", "ai", "ae",
	"ae", "ai", "ao", "au",
	"ea", "ei", "eo", "eu",
	"ia", "ie", "io", "iu",
	"oa", "oe", "oi", "ou",
	"ua", "ue", "ui", "uo",
}

func dypt() string {
	n := rand.Int() % len(dyp)
	return dyp[n]
}

func cv() string  { return c() + v() }
func vc() string  { return v() + c() }
func vcv() string { return v() + c() + v() }
func cvc() string { return c() + v() + c() }

var patterns = [][]int{
	{1, 3},
	{1, 3},
	{1, 3},
	{2, 4, 6},
	{2, 4, 6},
	{2, 4, -6},
	{1, 1, 6},
	{1, 1, 6},
	{1, 1, -6},
	{1, 1, -6, -7},
	{1, -3, -7},
	{1, -3, 2},
	{1, -3, 2},
	{1, -3, -2, -7},
	{1, 5, 2},
	{1, 5, 2},
	{1, 5, -2, -7},
}

var cl = []string{
	"br", "bb", "bbl", "bbr",
	"cr", "cl", "ccr",
	"dr", "dd", "dl",
	"fr", "fl", "ff", "ffr", "ffl",
	"gr", "gg", "ggr", "ggl",
	"kr", "ks",
	"lg", "lf", "lt", "ll", "lf", "lp",
	"mb", "mf", "mm",
	"nd", "nf", "nt", "ntr", "ndr", "ntl", "ndl",
	"pp", "pr", "pl",
	"rt", "rd", "rs", "rr", "rg", "rf", "rp", "rv",
	"ss", "sr", "sl",
	"tt", "tr",
	"vr"}

func getCl() string { return cl[rand.Intn(len(cl))] }

func roll(odd int, res int) bool { return rand.Int()%odd == res }
func rollString(odd int, res int, resTrue string, resFalse string) string {
	w := roll(odd, res)
	if w {
		return resTrue
	} else {
		return resFalse
	}
}

func CreateWord() string {
	res := ""
	pIdx := rand.Int() % len(patterns)
	//print (pIdx);
	pattern := patterns[pIdx]
	for _, i := range pattern {
		switch i {
		case -1:
			res += c() + rollString(4, 2, dypt(), v())
			break
		case -2:
			res += rollString(4, 2, dypt(), v()) + c()
			break
		case -3:
			res += c() + rollString(4, 2, dypt(), v()) + c()
			break
		case -4:
			res += rollString(4, 2, dypt(), v()) + c() + rollString(6, 2, dypt(), v())
			break
		case -6:
			res += rollString(3, 1, c(), getCl())
			break
		case -7:
			res += rollString(3, 2, dypt(), v())
			break
		case 1:
			res += cv()
			break
		case 2:
			res += vc()
			break
		case 3:
			res += cvc()
			break
		case 4:
			res += vcv()
			break
		case 5:
			res += getCl()
			break
		case 6:
			res += c()
			break
		case 7:
			res += v()
			break
		default:
			break
		}
	}
	return res
}
