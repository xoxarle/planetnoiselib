# planetnoiselib

`planetnoiselib` is a Go interface to the C++ `planetnoise` library, that can be found here: https://gitlab.com/maxlambertini/planetnoise

This distribution contains

* The appropriate binaries for Linux x64 (`libplanetnoise.so`) and Windows x64 (`libplanetnoise.dll`)
* The header file ( `dllmain.h`)
* Go library and test.
