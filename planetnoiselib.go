package planetnoiselib

/*
#cgo CFLAGS: -I./
#cgo LDFLAGS: -L./ -lplanetnoise -Wl,-rpath=./ -Wl,-rpath-link=./

#include <stdlib.h>

#include "dllmain.h"

*/
import "C"
import (
	"errors"
	"fmt"
	"unsafe"
)

type TextureBuilder struct {
	tb                C.CTextureBuilder
	Modules           []string
	Images            []string
	Heightmaps        []string
	HeightmapBuilders []string
	Renderers         []string
	DiffuseImage      string
	BumpImage         string
	SpecImage         string
	CloudImage        string
}

func NewTextureBuilder() (*TextureBuilder, error) {
	tb := TextureBuilder{}
	var err error
	tb.tb, err = C.tb_new()

	tb.HeightmapBuilders = []string{}
	tb.Images = []string{}
	tb.Modules = []string{}
	tb.Heightmaps = []string{}
	tb.Renderers = []string{}

	if err != nil {
		return nil, err
	}
	return &tb, nil
}

func (tb *TextureBuilder) Free() error {
	_, err := C.tb_free(tb.tb)
	return err
}

// void __declspec(dllexport) __stdcall tb_renderTexture(CTextureBuilder ptr, const char* pszPath );

func (tb *TextureBuilder) RenderTexture(path string) error {
	szPath := C.CString(path)
	defer func() {
		C.free(unsafe.Pointer(szPath))
	}()
	_, err := C.tb_renderTexture(tb.tb, szPath)
	return err
}

// void __declspec(dllexport) __stdcall tb_NewAbs (CTextureBuilder ptr, const char* szName, const char*  src1, BOOLEAN enableRandom );
func (tb *TextureBuilder) Abs(name string, src1 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewAbs(tb.tb, szName, szSrc1, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_setSize(CTextureBuilder ptr, int xSize, int ySize);
func (tb *TextureBuilder) SetSize(x int, y int) error {
	if x < 2 {
		return errors.New("x must be at least 2 pixel")
	}
	if y < 1 {
		return errors.New("x must be at least 1 pixel")
	}
	_, err := C.tb_setSize(tb.tb, C.int(x), C.int(y))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewAdd (CTextureBuilder ptr, const char* szName,
//
//	const char *   src1,
//	const char *   src2,
//	BOOLEAN enableRandom );
func (tb *TextureBuilder) Add(name string, src1 string, src2 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)
	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)

	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
	}()
	_, err := C.tb_NewAdd(tb.tb, szName, szSrc1, szSrc2, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewAvg (CTextureBuilder ptr, const char* szName,
//
//	const char *   src1,
//	const char *   src2,
//	BOOLEAN enableRandom );
func (tb *TextureBuilder) Avg(name string, src1 string, src2 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)
	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
	}()
	_, err := C.tb_NewAvg(tb.tb, szName, szSrc1, szSrc2, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewAvg4 (CTextureBuilder ptr, const char* szName,
//
//	const char *   src1,
//	const char *   src2,
//	const char *   src3,
//	const char *   src4, BOOLEAN enableRandom );
func (tb *TextureBuilder) Avg4(name string, src1 string, src2 string, src3 string, src4 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)
	szSrc3 := C.CString(src3)
	szSrc4 := C.CString(src4)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}
	res, e = tb.HasModule(src3)
	if !res {
		return e
	}
	res, e = tb.HasModule(src4)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
		C.free(unsafe.Pointer(szSrc3))
		C.free(unsafe.Pointer(szSrc4))
	}()
	_, err := C.tb_NewAvg4(tb.tb, szName, szSrc1, szSrc2, szSrc3, szSrc4, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewBillow (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,  int oct  ,  BOOLEAN enableRandom );
func (tb *TextureBuilder) Billow(name string, seed int, freq float64, lac float64, pers float64, oct int, enableRandom bool) error {

	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewBillow(
		tb.tb, szName, C.int(seed),
		C.double(freq), C.double(lac), C.double(pers),
		C.int(oct), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewBlend (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	const char *   src2,
//	const char *   ctl, BOOLEAN enableRandom );
func (tb *TextureBuilder) Blend(name string, src1 string, src2 string, ctl string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)
	szCtl := C.CString(ctl)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}
	res, e = tb.HasModule(ctl)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
		C.free(unsafe.Pointer(szCtl))
	}()
	_, err := C.tb_NewBlend(tb.tb, szName, szSrc1, szSrc2, szCtl, C.int(er))
	return err
}

// void EXPORT  tb_Select (CTextureBuilder ptr, const char* szName, const char *
//
//	src1, const char *   src2,
//	const char * ctl,
//	double uBound, double lBound, double value,
//	BOOLEAN enableRandom );
func (tb *TextureBuilder) Select(name string, src1 string, src2 string, ctl string,
	lBound float64, uBound float64, value float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)
	szCtl := C.CString(ctl)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}
	res, e = tb.HasModule(ctl)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
		C.free(unsafe.Pointer(szCtl))
	}()
	_, err := C.tb_Select(tb.tb, szName, szSrc1, szSrc2, szCtl, C.double(uBound), C.double(lBound), C.double(value), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewCheckerboard (CTextureBuilder ptr, const char* szName);
func (tb *TextureBuilder) Checkerboard(name string) error {
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szName := C.CString(name)

	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewCheckerboard(tb.tb, szName)
	return err
}

// void __declspec(dllexport) __stdcall tb_NewClamp (CTextureBuilder ptr, const char* szName, const char *   src1,
// double uBound, double lBound, BOOLEAN enableRandom );
func (tb *TextureBuilder) Clamp(name string, src1 string, ubound float64, lbound float64, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}

	tb.Modules = append(tb.Modules, name)

	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewClamp(tb.tb, szName, szSrc1, C.double(ubound), C.double(lbound), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewConst (CTextureBuilder ptr, const char* szName, double value,BOOLEAN enableRandom );
func (tb *TextureBuilder) Const(name string, value float64, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewConst(tb.tb, szName, C.double(value), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewCos (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
//
//	BOOLEAN enableRandom );
func (tb *TextureBuilder) Cos(name string, src1 string, freq float64, exp float64, value float64, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewCos(tb.tb, szName, szSrc1, C.double(freq), C.double(exp), C.double(value), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewCache (CTextureBuilder ptr, const char* szName, const char *   src1);
func (tb *TextureBuilder) Cache(name string, src1 string) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewCache(tb.tb, szName, szSrc1)
	return err
}

type CPointStruct struct {
	X float64
	Y float64
}

// void __declspec(dllexport) __stdcall tb_NewCurve (CTextureBuilder ptr, const char* szName, const char *   src1,
//     CPOINT *cPoints,
//     int numPoints,
//     BOOLEAN enableRandom );

func (tb *TextureBuilder) Curve(name string, src1 string, points []CPointStruct, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}

	var cpStruct []C.struct_CPoint
	for _, cps := range points {
		cpStruct = append(cpStruct, C.struct_CPoint{C.double(cps.X), C.double(cps.Y)})
	}

	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewCurve(tb.tb, szName, szSrc1, &cpStruct[0], C.int(len(cpStruct)), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewCylinders (CTextureBuilder ptr, const char* szName, double freq, BOOLEAN enableRandom );
func (tb *TextureBuilder) Cylinders(name string, freq float64, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}

	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewCylinders(tb.tb, szName, C.double(freq), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewDisplace (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	const char *   src2,
//	const char *   src3,
//	const char *  src4,
//	const char *   ctl, BOOLEAN enableRandom );
func (tb *TextureBuilder) Displace(name string, src1 string, src2 string, src3 string, src4 string, ctl string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)
	szSrc3 := C.CString(src3)
	szSrc4 := C.CString(src4)
	szCtl := C.CString(ctl)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}
	res, e = tb.HasModule(src3)
	if !res {
		return e
	}
	res, e = tb.HasModule(src4)
	if !res {
		return e
	}
	res, e = tb.HasModule(ctl)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
		C.free(unsafe.Pointer(szSrc3))
		C.free(unsafe.Pointer(szSrc4))
		C.free(unsafe.Pointer(szCtl))
	}()
	_, err := C.tb_NewDisplace(tb.tb, szName, szSrc1, szSrc2, szSrc3, szSrc4, szCtl, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewExponent (CTextureBuilder ptr, const char* szName,
// const char *   src1, double exp,
//
//	BOOLEAN enableRandom );
func (tb *TextureBuilder) Exponent(name string, src1 string, exp float64, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewExponent(tb.tb, szName, szSrc1, C.double(exp), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewInvert (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	BOOLEAN enableRandom );
func (tb *TextureBuilder) Invert(name string, src1 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewInvert(tb.tb, szName, szSrc1, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewMax (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	const char *   src2, BOOLEAN enableRandom );
func (tb *TextureBuilder) Max(name string, src1 string, src2 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
	}()
	_, err := C.tb_NewMax(tb.tb, szName, szSrc1, szSrc2, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewMin (CTextureBuilder ptr, const char* szName, const char *   src1, const char *   src2, BOOLEAN enableRandom );
func (tb *TextureBuilder) Min(name string, src1 string, src2 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
	}()
	_, err := C.tb_NewMin(tb.tb, szName, szSrc1, szSrc2, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewMultiply (CTextureBuilder ptr, const char* szName,
//
//	const char *   src1, const char *   src2, BOOLEAN enableRandom );
func (tb *TextureBuilder) Multiply(name string, src1 string, src2 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
	}()
	_, err := C.tb_NewMultiply(tb.tb, szName, szSrc1, szSrc2, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewPower (CTextureBuilder ptr, const char* szName, const char *
//
//	src1, const char *   src2, BOOLEAN enableRandom );
func (tb *TextureBuilder) Power(name string, src1 string, src2 string, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	szSrc2 := C.CString(src2)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}
	res, e = tb.HasModule(src2)
	if !res {
		return e
	}
	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
		C.free(unsafe.Pointer(szSrc2))
	}()
	_, err := C.tb_NewPower(tb.tb, szName, szSrc1, szSrc2, C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewPerlin (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac , double pers ,
//
//	int oct  ,  BOOLEAN enableRandom );
func (tb *TextureBuilder) Perlin(name string, seed int, freq float64, lac float64, pers float64, oct int, enableRandom bool) error {

	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewPerlin(
		tb.tb, szName, C.int(seed),
		C.double(freq), C.double(lac), C.double(pers),
		C.int(oct), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewRidgedMulti (CTextureBuilder ptr, const char* szName, int seed,  double freq , double lac ,
//
//	int oct  ,  BOOLEAN enableRandom );
func (tb *TextureBuilder) RidgedMulti(name string, seed int, freq float64, lac float64, oct int, enableRandom bool) error {

	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewRidgedMulti(
		tb.tb, szName, C.int(seed),
		C.double(freq), C.double(lac),
		C.int(oct), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewRidgedMulti2 (CTextureBuilder ptr, const char* szName, int seed,
//
//	 double freq , double lac ,
//
//		double gain, double exp, int oct,double offset, BOOLEAN enableRandom );
func (tb *TextureBuilder) RidgedMulti2(name string, seed int, freq float64, lac float64, gain float64, exp float64,
	offset float64, oct int, enableRandom bool) error {

	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewRidgedMulti2(
		tb.tb, szName, C.int(seed),
		C.double(freq), C.double(lac),
		C.double(gain), C.double(exp),
		C.int(oct), C.double(offset), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewRotatePoint (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	double x, double y, double z, BOOLEAN enableRandom );
func (tb *TextureBuilder) RotatePoint(name string, src1 string,
	x float64, y float64, z float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewRotatePoint(tb.tb, szName, szSrc1,
		C.double(x), C.double(y), C.double(z),
		C.int(er))
	return err
}

func (tb *TextureBuilder) ScalePoint(name string, src1 string,
	x float64, y float64, z float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewScalePoint(tb.tb, szName, szSrc1,
		C.double(x), C.double(y), C.double(z),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewScaleBias (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	double scale, double bias, BOOLEAN enableRandom );
func (tb *TextureBuilder) ScaleBias(name string, src1 string,
	scale float64, bias float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewScaleBias(tb.tb, szName, szSrc1,
		C.double(scale), C.double(bias),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewSin (CTextureBuilder ptr, const char* szName, const char *   src1, double freq, double exp, double value,
//
//	BOOLEAN enableRandom );
func (tb *TextureBuilder) Sin(name string, src1 string, freq float64, exp float64, value float64, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewSin(tb.tb, szName, szSrc1, C.double(freq), C.double(exp), C.double(value), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewSpheres (CTextureBuilder ptr, const char* szName,  double freq, BOOLEAN enableRandom );
func (tb *TextureBuilder) Spheres(name string,
	freq float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewSpheres(tb.tb, szName,
		C.double(freq),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewTranslatePoint (CTextureBuilder ptr, const char*
//
//	szName, const char *   src1,
//	double x, double y, double z, BOOLEAN enableRandom );
func (tb *TextureBuilder) TranslatePoint(name string, src1 string,
	x float64, y float64, z float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)
	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewTranslatePoint(tb.tb, szName, szSrc1,
		C.double(x), C.double(y), C.double(z),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewTurbulence (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
func (tb *TextureBuilder) Turbulence(name string, src1 string,
	seed int, freq float64, pow float64, rough float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewTurbulence(tb.tb, szName, szSrc1,
		C.int(seed), C.double(freq), C.double(pow), C.double(rough),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewTurbulence2 (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
func (tb *TextureBuilder) Turbulence2(name string, src1 string,
	seed int, freq float64, pow float64, rough float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewTurbulence2(tb.tb, szName, szSrc1,
		C.int(seed), C.double(freq), C.double(pow), C.double(rough),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewTurbulenceBillow (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
func (tb *TextureBuilder) TurbulenceBillow(name string, src1 string,
	seed int, freq float64, pow float64, rough float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewTurbulenceBillow(tb.tb, szName, szSrc1,
		C.int(seed), C.double(freq), C.double(pow), C.double(rough),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewTurbulenceRidged (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	int seed , double freq , double pow , double rough , BOOLEAN enableRandom );
func (tb *TextureBuilder) TurbulenceRidged(name string, src1 string,
	seed int, freq float64, pow float64, rough float64,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewTurbulenceRidged(tb.tb, szName, szSrc1,
		C.int(seed), C.double(freq), C.double(pow), C.double(rough),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewVoronoi (CTextureBuilder ptr, const char* szName,
//
//	int seed , double freq , double displ , BOOLEAN enableDispl , BOOLEAN enableRandom );
func (tb *TextureBuilder) Voronoi(name string,
	seed int, freq float64, displ float64, enableDispl bool,
	enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}
	ed := 1
	if !enableDispl {
		ed = 0
	}
	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_NewVoronoi(tb.tb, szName,
		C.int(seed), C.double(freq), C.double(displ), C.int(ed),
		C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_NewTerrace (CTextureBuilder ptr, const char* szName, const char *   src1,
//
//	CPOINT *cPoints, int numPoints, BOOLEAN invert, BOOLEAN enableRandom );
func (tb *TextureBuilder) Terrace(name string, src1 string, points []CPointStruct, invert bool, enableRandom bool) error {
	szName := C.CString(name)
	res1, e := tb.HasModule(name)
	if res1 {
		return e
	}

	szSrc1 := C.CString(src1)

	res, e := tb.HasModule(src1)
	if !res {
		return e
	}

	er := 1
	if !enableRandom {
		er = 0
	}

	iv := 0
	if invert {
		er = 1
	}

	var cpStruct []C.struct_CPoint
	for _, cps := range points {
		cpStruct = append(cpStruct, C.struct_CPoint{C.double(cps.X), C.double(cps.Y)})
	}

	tb.Modules = append(tb.Modules, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szSrc1))
	}()
	_, err := C.tb_NewTerrace(tb.tb, szName, szSrc1, &cpStruct[0], C.int(len(cpStruct)), C.int(iv), C.int(er))
	return err
}

// void __declspec(dllexport) __stdcall tb_createHeightMapDescriptor(CTextureBuilder ptr, const char* szName);
func (tb *TextureBuilder) CreateHeightmapDescriptor(name string) error {
	szName := C.CString(name)
	res1, e := tb.HasHeightMap(name)
	if res1 {
		return e
	}

	tb.Heightmaps = append(tb.Heightmaps, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_createHeightMapDescriptor(tb.tb, szName)
	return err
}

// void __declspec(dllexport) __stdcall tb_createImageDescriptor(CTextureBuilder ptr, const char* szName);

func (tb *TextureBuilder) CreateImageDescriptor(name string) error {
	szName := C.CString(name)
	res1, e := tb.HasImage(name)
	if res1 {
		return e
	}

	tb.Images = append(tb.Images, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_createImageDescriptor(tb.tb, szName)
	return err
}

// void __declspec(dllexport) __stdcall tb_createNoiseMapBuilderDescriptor(CTextureBuilder ptr, const char* szName, const char *   module, const char*  heightMap);
func (tb *TextureBuilder) CreateNoiseMapBuilderDescriptor(name string, module string, heightMap string) error {
	szName := C.CString(name)
	res1, e := tb.HasHeightMapBuilder(name)
	if res1 {
		return e
	}

	szModule := C.CString(module)

	res, e := tb.HasModule(module)
	if !res {
		return e
	}
	res, e = tb.HasHeightMap(heightMap)
	if !res {
		return e
	}
	szHeightmap := C.CString(heightMap)
	tb.HeightmapBuilders = append(tb.HeightmapBuilders, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szModule))
		C.free(unsafe.Pointer(szHeightmap))
	}()
	_, err := C.tb_createNoiseMapBuilderDescriptor(tb.tb, szName, szModule, szHeightmap)
	return err
}

type GradientInfo struct {
	Position float64
	Red      int
	Green    int
	Blue     int
	Alpha    int
}

type ColorInfo struct {
	Red   int
	Green int
	Blue  int
	Alpha int
}

// void __declspec(dllexport) __stdcall tb_createRendererDescriptor (
//
//	CTextureBuilder ptr,
//	const char* szName,
//	const char *heightMap ,
//	GRADIENTINFOSTRUCT *gradientInfo,
//	int numGradientInfo,
//	const char *destImage,
//	const char *backgroundImage ,
//	const char *alphaImage  ,
//	const char *bumpMap ,
//	double lightBrightness ,
//	double lightContrast ,
//	BOOLEAN enabledLight ,
//	BOOLEAN randomGradient ,
//	int bgRed,
//	int bgGreen,
//	int bgBlue,
//	int bgAlpha);
func (tb *TextureBuilder) CreateRendererDescriptor(name string, heightMap string,
	destImage string, backgroundImage string, alphaImage string, bumpMap string,
	lightBrightness float64, lightContrast float64,
	enabledLight bool, randomGradient bool,
	gradient []GradientInfo, backgroundColor *ColorInfo) error {
	szName := C.CString(name)
	res1, e := tb.HasRenderer(name)
	if res1 {
		return e
	}

	szHeightmap := C.CString(heightMap)
	res1, e = tb.HasHeightMap(heightMap)
	if !res1 {
		return e
	}

	szDestImage := C.CString(destImage)
	res1, e = tb.HasImage(destImage)
	if !res1 {
		return e
	}

	szBackgroundImage := C.CString(backgroundImage)
	if backgroundImage != "" {
		res1, e = tb.HasHeightMap(backgroundImage)
		if !res1 {
			return e
		}
	}

	szAlphaImage := C.CString(alphaImage)
	if alphaImage != "" {
		res1, e = tb.HasHeightMap(alphaImage)
		if !res1 {
			return e
		}
	}

	szBumpMap := C.CString(bumpMap)
	if bumpMap != "" {
		res1, e = tb.HasHeightMap(bumpMap)
		if !res1 {
			return e
		}
	}

	res, e := tb.HasHeightMap(heightMap)
	if !res {
		return e
	}
	res, e = tb.HasImage(destImage)
	if !res {
		return e
	}
	if backgroundImage != "" {
		res, e = tb.HasImage(backgroundImage)
		if !res {
			return e
		}
	}
	if alphaImage != "" {
		res, e = tb.HasImage(alphaImage)
		if !res {
			return e
		}
	}
	if bumpMap != "" {
		res, e = tb.HasHeightMap(bumpMap)
		if !res {
			return e
		}
	}

	i_enabledLight := C.int(0)
	i_randomGradient := C.int(0)
	if enabledLight {
		i_enabledLight = C.int(1)
	}
	if randomGradient {
		i_randomGradient = C.int(1)
	}

	pBackgroundImage := szBackgroundImage
	pAlphaImage := szAlphaImage
	pBumpMap := szBumpMap
	if backgroundImage == "" {
		pBackgroundImage = nil
	}
	if alphaImage == "" {
		pAlphaImage = nil
	}
	if bumpMap == "" {
		pBumpMap = nil
	}

	var gradientInfoStruct []C.struct_GradientInfoStruct
	for _, gs := range gradient {
		gradientInfoStruct = append(gradientInfoStruct, C.struct_GradientInfoStruct{
			C.double(gs.Position),
			C.int(gs.Red),
			C.int(gs.Green),
			C.int(gs.Blue),
			C.int(gs.Alpha),
		})
	}

	tb.Renderers = append(tb.Renderers, name)
	defer func() {
		C.free(unsafe.Pointer(szName))
		C.free(unsafe.Pointer(szDestImage))
		C.free(unsafe.Pointer(szHeightmap))
		C.free(unsafe.Pointer(szBackgroundImage))
		C.free(unsafe.Pointer(szAlphaImage))
		C.free(unsafe.Pointer(szBumpMap))
	}()
	_, err := C.tb_createRendererDescriptor(tb.tb, szName, szHeightmap, &gradientInfoStruct[0], C.int(len(gradientInfoStruct)),
		szDestImage, pBackgroundImage, pAlphaImage, pBumpMap,
		C.double(lightBrightness), C.double(lightContrast),
		i_enabledLight, i_randomGradient,
		C.int(backgroundColor.Red), C.int(backgroundColor.Red),
		C.int(backgroundColor.Blue), C.int(backgroundColor.Alpha))
	return err
}

// void __declspec(dllexport) __stdcall tb_setOutputName(CTextureBuilder ptr, const char* pszName );
func (tb *TextureBuilder) SetOutputName(name string) error {
	if name == "" {
		return errors.New("name should not be an empty string")
	}
	szName := C.CString(name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	_, err := C.tb_setOutputName(tb.tb, szName)
	return err
}

// int __declspec(dllexport) tb_HasModule(CTextureBuilder ptr, const char *szName) ;
func (tb *TextureBuilder) HasModule(name string) (bool, error) {
	szName := C.CString(name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	i, err := C.tb_HasModule(tb.tb, szName)
	if err != nil {
		return true, err
	} else {
		if i == 0 {
			return false, errors.New(fmt.Sprintf("Module %s not present", name))
		} else {
			return true, errors.New(fmt.Sprintf("Module %s already present", name))
		}
	}
}

// int __declspec(dllexport) tb_HasRenderer(CTextureBuilder ptr, const char *szName) ;
func (tb *TextureBuilder) HasRenderer(name string) (bool, error) {
	szName := C.CString(name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	i, err := C.tb_HasRenderer(tb.tb, szName)
	if err != nil {
		return true, err
	} else {
		if i == 0 {
			return false, errors.New(fmt.Sprintf("Renderer %s not present", name))
		} else {
			return true, errors.New(fmt.Sprintf("Renderer %s already present", name))
		}
	}
}

// int __declspec(dllexport) tb_HasImage(CTextureBuilder ptr, const char *szName) ;
func (tb *TextureBuilder) HasImage(name string) (bool, error) {
	szName := C.CString(name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	i, err := C.tb_HasImage(tb.tb, szName)
	if err != nil {
		return true, err
	} else {
		if i == 0 {
			return false, errors.New(fmt.Sprintf("Image %s not present", name))
		} else {
			return true, errors.New(fmt.Sprintf("Image %s already present", name))
		}
	}
}

// int __declspec(dllexport) tb_HasHeightMap(CTextureBuilder ptr, const char *szName);
func (tb *TextureBuilder) HasHeightMap(name string) (bool, error) {
	szName := C.CString(name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	i, err := C.tb_HasHeightMap(tb.tb, szName)
	if err != nil {
		return true, err
	} else {
		if i == 0 {
			return false, errors.New(fmt.Sprintf("Heightmap %s not present", name))
		} else {
			return true, errors.New(fmt.Sprintf("Heightmap %s already present", name))
		}
	}
}

// int __declspec(dllexport) tb_HasHeightMapBuilder(CTextureBuilder ptr, const char *szName);
func (tb *TextureBuilder) HasHeightMapBuilder(name string) (bool, error) {
	szName := C.CString(name)
	defer func() {
		C.free(unsafe.Pointer(szName))
	}()
	i, err := C.tb_HasHeightMapBuilder(tb.tb, szName)
	if err != nil {
		return true, err
	} else {
		if i == 0 {
			return false, errors.New(fmt.Sprintf("HeightMapBuilder %s not present", name))
		} else {
			return true, errors.New(fmt.Sprintf("HeightMapBuilder %s already present", name))
		}
	}
}

// void __declspec(dllexport)tb_AddRandomFactors(CTextureBuilder ptr, const double *iRandomFactors, int numFactors );
func (tb *TextureBuilder) AddRandomFactors(randomFactors []float64) error {

	factors := make([]C.double, len(randomFactors))
	for _, v := range randomFactors {
		factors = append(factors, C.double(v))
	}

	_, err := C.tb_AddRandomFactors(tb.tb, &factors[0], C.int(len(factors)))
	return err
}

// void __declspec(dllexport) __stdcall createParamTexture(
//
//	BOOLEAN bEarthlike,
//	const char * pszTexture1,
//	const char * pszTexture2,
//	double seaLevel,
//	double iceLevel,
//	int width,
//	const char * pszPath,
//	const char *pszFilename,
//	char *szRes);
func CreateParamTexture(bEarthlike bool, texture1 string, texture2 string,
	seaLevel float64, icelevel float64,
	width int, path string, filename string, res string) error {
	t1 := C.CString(texture1)
	t2 := C.CString(texture2)
	p := C.CString(path)
	fn := C.CString(filename)
	rs := C.CString(res)
	er := 1
	if !bEarthlike {
		er = 0
	}
	defer func() {
		C.free(unsafe.Pointer(t1))
		C.free(unsafe.Pointer(t2))
		C.free(unsafe.Pointer(p))
		C.free(unsafe.Pointer(fn))
		C.free(unsafe.Pointer(rs))
	}()
	_, err := C.createParamTexture(C.int(er), t1, t2, C.double(seaLevel), C.double(icelevel), C.int(width), p, fn, rs)
	return err
}

// void EXPORT  tb_RenderTextureFromJsonString(CTextureBuilder ptr, const char *pszJson, const char* pszPath );
func (tb *TextureBuilder) RenderTextureFromJsonString(jsonData string, path string) error {
	pszJsonData := C.CString(jsonData)
	pszPath := C.CString(path)
	defer func() {
		C.free(unsafe.Pointer(pszJsonData))
		C.free(unsafe.Pointer(pszPath))
	}()
	_, err := C.tb_RenderTextureFromJsonString(tb.tb, pszJsonData, pszPath)
	if err != nil {
		return err
	} else {
		return nil
	}
}

// void __declspec(dllexport) __stdcall createTexture(const char* pszJsonFile, const char * pszPath,
// const char *pszFilename, char *szRes);
func CreateTexture(jsonFile string, path string, filename string) (string, error) {
	pszJsonFile := C.CString(jsonFile)
	pszPath := C.CString(path)
	pszFilename := C.CString(filename)
	defer func() {
		C.free(unsafe.Pointer(pszFilename))
		C.free(unsafe.Pointer(pszJsonFile))
		C.free(unsafe.Pointer(pszPath))
	}()
	cstr, err := C.createTexture(pszJsonFile, pszPath, pszFilename)
	if err != nil {
		return "", err
	} else {
		res := C.GoString(cstr)
		C.free(unsafe.Pointer(cstr))
		return res, nil
	}
}

// void EXPORT  tb_setColorMap(CTextureBuilder ptr, const char  *pszTexture1);
func (tb *TextureBuilder) SetColorMap(image string) error {
	res, _ := tb.HasImage(image)
	if !res {
		return errors.New(fmt.Sprintf("Image %s must exist\n", image))
	}
	pszImage := C.CString(image)
	defer func() {
		C.free(unsafe.Pointer(pszImage))
	}()

	C.tb_setColorMap(tb.tb, pszImage)
	return nil
}

// void EXPORT  tb_setBumpMap(CTextureBuilder ptr, const char  *pszTexture1);
func (tb *TextureBuilder) SetBumpMap(image string) error {
	res, _ := tb.HasImage(image)
	if !res {
		return errors.New(fmt.Sprintf("Image %s must exist\n", image))
	}
	pszImage := C.CString(image)
	defer func() {
		C.free(unsafe.Pointer(pszImage))
	}()
	C.tb_setBumpMap(tb.tb, pszImage)
	return nil
}

// void EXPORT  tb_setSpecMap(CTextureBuilder ptr, const char  *pszTexture1);
func (tb *TextureBuilder) SetSpecMap(image string) error {
	res, _ := tb.HasImage(image)
	if !res {
		return errors.New(fmt.Sprintf("Image %s must exist\n", image))
	}
	pszImage := C.CString(image)
	defer func() {
		C.free(unsafe.Pointer(pszImage))
	}()
	C.tb_setSpecMap(tb.tb, pszImage)
	return nil
}

// void EXPORT  tb_setCloudMap(CTextureBuilder ptr, const char  *pszTexture1);
func (tb *TextureBuilder) SetCloudMap(image string) error {
	res, _ := tb.HasImage(image)
	if !res {
		return errors.New(fmt.Sprintf("Image %s must exist\n", image))
	}
	pszImage := C.CString(image)
	defer func() {
		C.free(unsafe.Pointer(pszImage))
	}()
	C.tb_setCloudMap(tb.tb, pszImage)
	return nil
}

// char * __declspec(dllexport) tb_ToJsonString(CTextureBuilder ptr);
func (tb *TextureBuilder) ToJsonString() (string, error) {

	cstr, err := C.tb_ToJsonString(tb.tb)
	if err != nil {
		return "", err
	} else {
		res := C.GoString(cstr)
		C.free(unsafe.Pointer(cstr))
		return res, nil
	}
}

var JsonPresets = []string{
	"OUTRE2",
	"OUTRE3",
	"DESERT",
	"OUTRE",
	"PREGARDEN2",
	"FUNKYCLOUD",
	"EARTHLIKE_12",
	"ROCKBALL_02",
	"PREGARDEN3",
	"CANYON_01",
	"EARTHLIKE_ISLAND",
	//"GASGIANT4",
	"POSTGARDEN",
	"HOTHOUSE",
	"DESERT_CREAM",
	"ICEBALL",
	"GLACIER",
	"GASGIANT3",
	"PREGARDEN",
	"GASGIANT22",
	"OUTRE4",
	"EARTHLIKE_01",
	"ROCKBALL_03",
	"POSTGARDEN_3",
	"MULTILAYERED_EARTHLIKERND",
	"EARTHLIKE_01_1",
	"EARTHLIKE_02",
	"GASGIANT5",
	"DESERT2",
	"CLOUD",
	"GASGIANTOK",
	"ROCKBALL",
	"EARTHLIKE_04",
	"EARTHLIKE_10",
	"CHUNK",
	"EARTHLIKE_11",
	"GASGIANT21",
	"ALIENPEAKSVORONOI",
	"GASGIANTORIGINALOK",
	"GASGIANT2OK",
	"EARTHLIKE_REALISTIC",
	"DESERT_REDDISH",
	"EARTHLIKE_03",
}

// char * EXPORT GetJsonPreset(const char *szName)
func GetJsonPreset(name string) (string, error) {
	json, ok := PlanetTemplates[name]
	fmt.Printf("Reading template... %s", name)
	if !ok {
		return "", errors.New("No Json Template found for key " + name)
	} else {
		fmt.Println("Returning jsonic", json)
		return json, nil
	}
}

// void __declspec(dllexport) tb_PrepareTexture(CTextureBuilder ptr);
func (tb *TextureBuilder) PrepareTexture() error {
	_, err := C.tb_PrepareTexture(tb.tb)
	return err
}

// void __declspec(dllexport) tb_WriteTextureToPath(CTextureBuilder ptr, const char* szPath);
func (tb *TextureBuilder) WriteTexture(path string) error {
	pszPath := C.CString(path)
	defer func() {
		C.free(unsafe.Pointer(pszPath))
	}()
	_, err := C.tb_WriteTextureToPath(tb.tb, pszPath)
	return err
}

// void __declspec(dllexport) tb_LoadTextureFromJsonFile(CTextureBuilder ptr, const char* szPath);
func (tb *TextureBuilder) LoadTextureFromJsonFile(path string) error {
	pszPath := C.CString(path)
	defer func() {
		C.free(unsafe.Pointer(pszPath))
	}()
	_, err := C.tb_LoadTextureFromJsonFile(tb.tb, pszPath)
	return err
}

// void __declspec(dllexport) tb_LoadTextureFromJsonString(CTextureBuilder ptr, const char* szString);
func (tb *TextureBuilder) LoadTextureFromJsonString(path string) error {
	pszPath := C.CString(path)
	defer func() {
		C.free(unsafe.Pointer(pszPath))
	}()
	_, err := C.tb_LoadTextureFromJsonString(tb.tb, pszPath)
	return err
}
